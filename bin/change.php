<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   change
 * @since     2014.03.27.
 */
require_once(dirname(dirname(__FILE__)) . '/library/Change.php');

if ('cli' !== substr(php_sapi_name(), 0, 3)) {
    throw new Exception('Use it cli.');
}

$price = mt_rand(1, 50000);

if (isset($argv[1])) {
    $pay = $argv[1];

    if (isset($argv[2])) {
        $price = $argv[2];
    }
} else {
    $pay = $price + mt_rand(0, 50000);
}

$change = Change::factory(Change::LOCALE_HU);
$change->setPrice($price)->setPay($pay);

echo $change . PHP_EOL;
print_r($change->toArray());