<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   change
 * @since     2014.03.27.
 */

/**
 * Class Change_Exception
 */
class Change_Exception extends Exception {
} 