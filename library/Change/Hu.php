<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   change
 * @since     2014.03.27.
 */
require_once(dirname(__FILE__) . '/Abstract.php');
require_once(dirname(__FILE__) . '/Exception.php');

/**
 * Class Change_Hu
 */
class Change_Hu extends Change_Abstract {
    /**
     * @var string
     */
    protected $currency = 'Ft';

    /**
     * @var array
     */
    protected $banknotes = array(
            20000,
            10000,
            5000,
            2000,
            1000,
            500,
            200,
            100,
            50,
            20,
            10,
            5
    );

    /**
     * Handle left-over money.
     *
     * @param int $change
     *
     * @return void
     * @throws Change_Exception
     */
    protected function handleLeftOver($change) {
        if ($change > 2 and $change < 5) {
            if (!isset($this->change[5])) {
                $this->change[5] = 0;
            }

            ++$this->change[5]; // TODO optimalize change
        } elseif ($change < 0 or $change > 5) {
            throw new Change_Exception('Wrong calculation. Left-over: ' . $change);
        }
    }

    /**
     * Returns change money as a string.
     *
     * @return string
     */
    public function __toString() {
        $change = $this->getPay() - $this->getPrice();

        return number_format($change, 0, ',', ' ') . ' ' . $this->getCurrency();
    }

}
