<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   change
 * @since     2014.03.27.
 */
require_once(dirname(__FILE__) . '/Exception.php');

/**
 * Class Change_Abstract
 */
abstract class Change_Abstract {
    /**
     * @var string
     */
    protected $currency = '';

    /**
     * @var int
     */
    protected $price = 0;

    /**
     * @var int
     */
    protected $pay = 0;

    /**
     * @var array
     */
    protected $change = array();

    /**
     * @var array
     */
    protected $banknotes = array();


    /**
     * Constructor
     *
     * @param int $price
     * @param int $pay
     */
    public function __construct($price = 0, $pay = 0) {
        $this->setPrice($price);
        $this->setPay($pay);

        rsort($this->banknotes);
    }

    /**
     * Returns currency.
     *
     * @return string
     */
    final public function getCurrency() {
        return $this->currency;
    }

    /**
     * Returns paid money.
     *
     * @return int
     */
    final public function getPay() {
        return $this->pay;
    }

    /**
     * Sets paid money.
     *
     * @param int $pay
     *
     * @return $this
     */
    final public function setPay($pay) {
        $this->pay = (int)$pay;

        return $this;
    }

    /**
     * Returns price.
     *
     * @return int
     */
    final public function getPrice() {
        return $this->price;
    }

    /**
     * Sets price.
     *
     * @param int $price
     *
     * @return $this
     */
    final public function setPrice($price) {
        $this->price = (int)$price;

        return $this;
    }
    /**
     * Returns change money as an array.
     *
     * @return array
     */
    public function toArray() {
        if (count($this->change) < 1) {
            $result = $this->calculateChange();
            krsort($this->change, SORT_NUMERIC);

            if (0 !== $result) {
                $this->handleLeftOver($result);
            }
        }

        return $this->change;
    }

    /**
     * Calculate change money.
     *
     * @return int
     * @throws Change_Exception
     */
    final protected function calculateChange() {
        $change = $this->pay - $this->price;

        if ($change < 0) {
            throw new Change_Exception('Too less pay.');
        }

        while ($change > 0) {
            $banknote = $this->getNextBanknote($change);

            if (!$banknote) {
                break;
            }

            $this->change[$banknote] = floor($change / $banknote);
            $change = $change % $banknote;
        }

        return $change;
    }

    /**
     * Returns next banknote.
     *
     * @param int $changePart
     *
     * @return null
     */
    final protected function getNextBanknote($changePart) {
        foreach ($this->banknotes as $banknote) {
            if ($banknote <= $changePart) {
                return $banknote;
            }
        }

        return null;
    }

    /**
     * Handle left-over money.
     *
     * @param int $change
     *
     * @return void
     */
    abstract protected function handleLeftOver($change);

    /**
     * Returns change money as a string.
     *
     * @return string
     */
    abstract public function __toString();
}
