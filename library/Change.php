<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   change
 * @since     2014.03.27.
 */
require_once(dirname(__FILE__) . '/Change/Exception.php');

/**
 * Class Change
 */
class Change {
    const LOCALE_HU = 'hu';
    /**
     * Constructor
     */
    protected final function __construct() {

    }

    /**
     * Makes a Change class.
     *
     * @param string $locale
     *
     * @static
     * @return Change_Abstract
     */
    public static function factory($locale) {
        $locale = ucfirst(strtolower($locale));
        $class = 'Change_' . $locale;

        require_once(dirname(__FILE__) . '/Change/' . $locale . '.php');

        return new $class();
    }

    /**
     * Returns change money as an array.
     *
     * @param int    $price
     * @param int    $pay
     * @param string $locale
     *
     * @static
     * @return array
     */
    public static function getChange($price, $pay, $locale = self::LOCALE_HU) {
        $instance = self::factory($locale);
        $instance->setPrice($price);
        $instance->setPay($pay);

        return $instance->toArray();
    }
}
